# PropogateException-in-Java



import javax.swing.JOptionPane;

public class PropagateException {
 
 public static int getDepend() throws NumberFormatException {
  String numStr = JOptionPane.showInputDialog("Number of dependents:");
  return Integer.parseInt(numStr);
}
  // postcondition: Calls getDepend() and handles its exceptions.
  public static void main(String[] args) {
  
   int children = 1;
   try {
    children = getDepend();
   }
   catch(NumberFormatException ex) {
   // Handle number format exception
   JOptionPane.showInputDialog(null, "Invalid integer - default is 1", "Error", JOptionPane.ERROR_MESSAGE);
   ex.printStackTrace();
  }
 }
}    
  
